---
title: Ross Tapson
subtitle:  
layout: default
modal-id: 9
date: 2014-07-16
img: Rossclip.jpg
thumbnail: Rossclip_thumb.jpg
alt: image-alt
project-date: April 2014
client: Start Bootstrap
category: Special
description:
secondary: endorsements
---

<!-- html sytax to include image and adjust size ... -->

#### [Ross Tapson](https://www.facebook.com/ross.tapson), guitarist at [Templar Funk](https://www.facebook.com/pg/templarfunkband) and other projects plays a semi-hollow Amyris.

<img src="img/endorse/Ross/Rossclip-s.jpg" alt="Drawing" style="width: 32.5;"/>
