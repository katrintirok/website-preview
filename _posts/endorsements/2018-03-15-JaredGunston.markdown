---
title: Jared Gunston
subtitle:  
layout: default
modal-id: 11
date: 2014-07-14
img: JaredGunston.jpg
thumbnail: JaredGunston_thumb.jpg
alt: image-alt
project-date: April 2014
client: Start Bootstrap
category: Special
description:
secondary: endorsements
---

<!-- html sytax to include image and adjust size ... -->

#### [Jared Gunston](https://www.facebook.com/jaredgunstontv), guitarist at [Inceptio](https://www.facebook.com/inceptioZA) plays a Xyris drop top. Check out [Jared Gunston TV](https://www.youtube.com/user/ToneYardMusic/featured) for some sound tests.

<img src="img/endorse/Jared/JaredGunston-s.jpg" alt="Drawing"/>
